package com.example.emroze.gnexcel.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emroze.gnexcel.Main2Activity;
import com.example.emroze.gnexcel.R;

import net.cachapa.expandablelayout.ExpandableLayout;

public class exam_name extends AppCompatActivity {

    private ExpandableLayout expandableLayout0;
    DatePicker d1;
    TextView tv1,tv_pick;
    Button btn_pick,btn_ok;
    String exam_date="";
    EditText ex_name,f_marks;

    public static String PREFS_NAME="digits_insert";

    String BASE_URL = "http://gonitniketan.com/";


    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_name);

        //Toast.makeText(this, "exam_name", Toast.LENGTH_SHORT).show();

        pd = new ProgressDialog(exam_name.this);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv_pick = (TextView) findViewById(R.id.tv_pick);

        btn_pick = (Button) findViewById(R.id.btn_pick_date);
        btn_ok = (Button) findViewById(R.id.btn_ok);

        d1 = (DatePicker) findViewById(R.id.datePicker);

        ex_name = (EditText) findViewById(R.id.et_exam_name);

        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        tv_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        btn_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = d1.getDayOfMonth();
                int month = d1.getMonth() + 1;
                int year = d1.getYear();

                exam_date = String.valueOf(day) + " "+String.valueOf(month)+" "+String.valueOf(year);

                tv_pick.setText(exam_date);
                expandableLayout0.collapse();

            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!exam_date.equals("")){

                    editor.putString("exam_date",exam_date);
                    editor.putString("exam_name",ex_name.getText().toString());
                    editor.commit();
                    Intent i = new Intent(exam_name.this,choose_c_b_t.class);
                    startActivity(i);
                    //finish();

                }
            }
        });

    }
}
