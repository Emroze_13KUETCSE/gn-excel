package com.example.emroze.gnexcel.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.emroze.gnexcel.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.droidsonroids.gif.GifTextView;

public class choose_pre_exam_list extends AppCompatActivity {

    public static String PREFS_NAME="digits_insert";

    GifTextView g1,g2,g3;

    TextView tv_class,tv_batch,tv_7,tv_8,tv_9,tv_10,tv_7b,tv_8b,tv_9b,tv_12b,tv_3b,tv_4b,tv_5b,tv_730b,tv_time,tv_sat,tv_sun;

    Button btn_search;

    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2;

    String search_class , search_batch, sat_sun, batch_time ="";

    LinearLayout ll,ll_d;

    private ExpandableLayout expandableLayout5;
    DatePicker d1;
    TextView tv1,tv_pick,tv_total;
    Button btn_pick,btn_ok;
    String exam_date="";


    String BASE_URL = "http://gonitniketan.com/";

    String showUrl = "http://gonitniketan.com/jason1.php";
    String showUrl2 = "http://gonitniketan.com/json1.php";

    RequestQueue requestQueue,requestQueue1,requestQueue2;

    String[] exam_name1;
    String exam_date1;
    String[] exam_f_name1;
    int aa = 0;
    int len = 0;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_pre_exam_list);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        Toast.makeText(this, "choose", Toast.LENGTH_SHORT).show();

        class_batch_select();

        pd = new ProgressDialog(this);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv_pick = (TextView) findViewById(R.id.tv_pick);
        tv_total = (TextView) findViewById(R.id.tv_total);


        btn_pick = (Button) findViewById(R.id.btn_pick_date);
        btn_ok = (Button) findViewById(R.id.btn_ok);

        d1 = (DatePicker) findViewById(R.id.datePicker);
        expandableLayout5 = (ExpandableLayout) findViewById(R.id.expandable_layout_5);

        tv_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout5.isExpanded()) {
                    expandableLayout5.collapse();
                }
                else {
                    expandableLayout5.expand();
                }
            }
        });

        btn_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = d1.getDayOfMonth();
                int month = d1.getMonth() + 1;
                int year = d1.getYear();

                exam_date = String.valueOf(day) + " "+String.valueOf(month)+" "+String.valueOf(year);

                tv_pick.setText(exam_date);
                expandableLayout5.collapse();

            }
        });


        tv_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i = new Intent(getApplicationContext(),exam_list.class);
                startActivity(i);*/
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batch_time = search_batch+" "+sat_sun;

                if(!search_class.equals("")){

                    pd.setMessage("Loading...");
                    pd.show();

                    etc(search_class,exam_date);

                }
                else if(!search_class.equals("") && !exam_date.equals("")){

                    pd.setMessage("Loading...");
                    pd.show();

                    etc(search_class,exam_date);

                }
                else {
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);
                    Toast.makeText(getApplicationContext(), "At least fill up Class", Toast.LENGTH_SHORT).show();
                }





            }
        });

    }

    public void class_batch_select(){
        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        g2 = (GifTextView) findViewById(R.id.btn_gif2);
        g3 = (GifTextView) findViewById(R.id.btn_gif3);


        tv_class = (TextView) findViewById(R.id.tv_class);
        tv_batch = (TextView) findViewById(R.id.tv_batch);
        tv_7 = (TextView) findViewById(R.id.tv_7);
        tv_8 = (TextView) findViewById(R.id.tv_8);
        tv_9 = (TextView) findViewById(R.id.tv_9);
        tv_10 = (TextView) findViewById(R.id.tv_10);
        tv_7b = (TextView) findViewById(R.id.tv_7b);
        tv_8b = (TextView) findViewById(R.id.tv_8b);
        tv_9b = (TextView) findViewById(R.id.tv_9b);
        tv_12b = (TextView) findViewById(R.id.tv_12b);
        tv_3b = (TextView) findViewById(R.id.tv_3b);
        tv_4b = (TextView) findViewById(R.id.tv_4b);
        tv_5b = (TextView) findViewById(R.id.tv_5b);
        tv_730b = (TextView) findViewById(R.id.tv_730b);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        tv_class.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_7.setTypeface(type1);
        tv_8.setTypeface(type1);
        tv_9.setTypeface(type1);
        tv_10.setTypeface(type1);
        tv_7b.setTypeface(type1);
        tv_8b.setTypeface(type1);
        tv_9b.setTypeface(type1);
        tv_12b.setTypeface(type1);
        tv_3b.setTypeface(type1);
        tv_4b.setTypeface(type1);
        tv_5b.setTypeface(type1);
        tv_730b.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_sat.setTypeface(type1);
        tv_sun.setTypeface(type1);

        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);

        ll = (LinearLayout) findViewById(R.id.ll);

        ll.setVisibility(View.INVISIBLE);

        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        //select class
        tv_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "7";
                tv_class.setText("Class 7");
                expandableLayout0.collapse();
            }
        });
        tv_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "8";
                tv_class.setText("Class 8");
                expandableLayout0.collapse();
            }
        });
        tv_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "9";
                tv_class.setText("Class 9");
                expandableLayout0.collapse();
            }
        });
        tv_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "1";
                tv_class.setText("Class 10");
                expandableLayout0.collapse();
            }
        });



        //select batch
        tv_7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0700am";
                tv_batch.setText("7 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0800am";
                tv_batch.setText("8 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0900am";
                tv_batch.setText("9 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "1200pm";
                tv_batch.setText("12 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0300pm";
                tv_batch.setText("3 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0400pm";
                tv_batch.setText("4 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0500pm";
                tv_batch.setText("5 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_730b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0730pm";
                tv_batch.setText("7 30 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });


        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });


        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sat";
                tv_time.setText("SAT");
                expandableLayout2.collapse();
            }
        });

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sun";
                tv_time.setText("SUN");
                expandableLayout2.collapse();
            }
        });
    }

    private void showJSON(String response,final String s_class,final String e_date){
        String name="";
        String address="";
        String vc = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONArray("students");
            int len = result.length();
            for(int i=0; i< len;i++){
                JSONObject collegeData = result.getJSONObject(i);
                name = collegeData.getString("exam_date");
                address = collegeData.getString("class");
                vc = collegeData.getString("exam_name");

                if(name.equals(e_date) && address.equals(s_class)) {
                    Toast.makeText(this, vc, Toast.LENGTH_SHORT).show();
                }
            }

            //vc = collegeData.getString(Config.KEY_VC);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //textViewResult.setText("Name:\t"+name+"\nAddress:\t" +address+ "\nVice Chancellor:\t"+ vc);
    }

    public void etc(final String s_class, final String e_date){

        requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i <= std.length(); i++) {
                        if(i == std.length()){
                            exam_f_name1 = new String[len];
                            exam_name1 = new String[len];
                            break;
                        }
                        JSONObject student = std.getJSONObject(i);
                        if(e_date == ""){
                            if(student.getString("class").equals(s_class)){
                                len++;
                            }
                        }
                        else {
                            if(student.getString("exam_date").equals(e_date) && student.getString("class").equals(s_class)){
                                len++;
                            }
                        }


                    }
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        if(e_date == ""){
                            if(student.getString("class").equals(s_class)){
                                //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                                exam_f_name1[aa] = student.getString("exam_name");
                                String temp_ex_name="";
                                for(int j = 0; j < exam_f_name1[aa].length();j++){
                                    if(exam_f_name1[aa].charAt(j) == 's' && exam_f_name1[aa].charAt(j+1) == 'k'){
                                        break;
                                    }
                                    temp_ex_name = temp_ex_name.concat(String.valueOf(exam_f_name1[aa].charAt(j)));
                                }
                                exam_name1[aa] = temp_ex_name;
                                aa++;

                            }
                        }
                        else {
                            if(student.getString("exam_date").equals(e_date) && student.getString("class").equals(s_class)){
                                //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                                exam_f_name1[aa] = student.getString("exam_name");
                                String temp_ex_name="";
                                for(int j = 0; j < exam_f_name1[aa].length();j++){
                                    if(exam_f_name1[aa].charAt(j) == 's' && exam_f_name1[aa].charAt(j+1) == 'k'){
                                        break;
                                    }
                                    temp_ex_name = temp_ex_name.concat(String.valueOf(exam_f_name1[aa].charAt(j)));
                                }
                                exam_name1[aa] = temp_ex_name;
                                aa++;

                            }
                        }


                    }

                    Intent intent = new Intent(getApplicationContext(),exam_search_list.class);
                    intent.putExtra("ex_course",s_class);
                    intent.putExtra("ex_name",exam_name1);
                    intent.putExtra("ex_f_name",exam_f_name1);
                    intent.putExtra("len",len);
                    intent.putExtra("length",len);
                    startActivity(intent);
                    len = 0;
                    aa=0;
                    pd.hide();

                } catch (JSONException e) {

                    e.printStackTrace();

                    pd.hide();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue1.add(jsonObjectRequest2);
        requestQueue1.getCache().clear();
        requestQueue1.getCache().remove(showUrl2);

        for(int aaaa = 0; aaaa < len; aaaa++){
            Toast.makeText(this, exam_name1[aaaa], Toast.LENGTH_SHORT).show();
        }


    }
}
