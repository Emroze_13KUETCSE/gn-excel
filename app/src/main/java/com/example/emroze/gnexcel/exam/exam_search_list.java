package com.example.emroze.gnexcel.exam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.emroze.gnexcel.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class exam_search_list extends AppCompatActivity {
    public static String PREFS_NAME="digits_insert";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    String ex_name;

    Cursor cursor1;
    Cursor cursor;

    Cursor cursor_marks_result;

    String[] exam_name1;
    String exam_date1;
    String[] exam_f_name1;
    int length_array = 0;

    String[] name;
    String[] regi;
    String[] marks;

    String[][] joint_marks;
    String[][] joint_name;

    int total =0;

    search_list_adapter listviewadapter;
    List<search_list_element> list_data = new ArrayList<search_list_element>();


    String BASE_URL = "http://gonitniketan.com/";

    String showUrl2 = "http://gonitniketan.com/json.php";
    String showUrl3 = "http://gonitniketan.com/jason1.php";


    RequestQueue requestQueue1,requestQueue2;

    int len = 0;
    int aa = 0;

    String classs = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_search_list);

        Intent intent = getIntent();
        length_array = intent.getIntExtra("len",0);
        classs = intent.getStringExtra("ex_course");
        //Toast.makeText(this, String.valueOf(length_array), Toast.LENGTH_SHORT).show();
        exam_name1 = new String[length_array];
        exam_f_name1 = new String[length_array];
        exam_name1 = intent.getStringArrayExtra("ex_name");
        exam_f_name1 = intent.getStringArrayExtra("ex_f_name");

        tv1 = (TextView) findViewById(R.id.tv_class_name);

        lv = (ListView) findViewById(R.id.sms_lv);
        pd = new ProgressDialog(exam_search_list.this);

        /*for(int aaaa = 0; aaaa < length_array; aaaa++){
            Toast.makeText(this, exam_name1[aaaa], Toast.LENGTH_SHORT).show();
        }*/

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();
        final String match_date = preference.getString("s_date","").toString();


        for (int i = 0; i < length_array; i++) {
            search_list_element worldpopulation = new search_list_element(exam_f_name1[i],
                    exam_name1[i], match_date);
            list_data.add(worldpopulation);
        }

        listviewadapter = new search_list_adapter(this, R.layout.s_by_exam_list,
                list_data);

        // Binds the Adapter to the ListView
        lv.setAdapter((ListAdapter) listviewadapter);




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv = (TextView) view.findViewById(R.id.tv_hide);
                ex_name = tv.getText().toString();
                TextView del = (TextView) view.findViewById(R.id.tv_del);
                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                editor.putString("exam_name",ex_name);
                editor.commit();
                final String date = tvv.getText().toString();

                Intent intent = new Intent(getApplicationContext(),upload_marks_pre.class);
                intent.putExtra("ex_course",classs);
                startActivity(intent);


            }
        });
    }

    public void etc(final String s_class,final String ex_name){

        requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i <= std.length(); i++) {
                        if(i == std.length()){
                            regi = new String[len];
                            name = new String[len];
                            marks = new String[len];
                            joint_marks = new  String[len][2];
                            joint_name = new  String[len][2];


                            //Toast.makeText(exam_search_list.this, String.valueOf(len), Toast.LENGTH_SHORT).show();
                            for (int ii = 0; ii < std.length(); ii++) {
                                JSONObject student = std.getJSONObject(ii);

                                if(student.getString("course").equals(s_class)){

                                    joint_marks[aa][0] = student.getString("regi");
                                    joint_marks[aa][1] = student.getString(ex_name);
                                    //name[aa] = "no";
                                    aa++;
                                    //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                                }

                            }
                            //array sorting
                            Arrays.sort(joint_marks, new Comparator<String[]>() {
                                @Override
                                public int compare(final String[] entry1, final String[] entry2) {
                                    final String time1 = entry1[0];
                                    final String time2 = entry2[0];
                                    return time1.compareTo(time2);
                                }
                            });

                            etc1(s_class);
                            break;
                        }
                        JSONObject student = std.getJSONObject(i);

                        if( student.getString("course").equals(s_class)){
                            len++;
                        }

                    }


                } catch (JSONException e) {

                    e.printStackTrace();

                    pd.hide();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue1.add(jsonObjectRequest2);
        requestQueue1.getCache().clear();
        requestQueue1.getCache().remove(showUrl2);


    }
    public void etc1(final String s_class){

        aa=0;
        requestQueue2 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest3 = new JsonObjectRequest(Request.Method.POST,
                showUrl3, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {

                        JSONObject student = std.getJSONObject(i);

                        if(student.getString("course").equals(s_class)){

                            joint_name[aa][0] = student.getString("regi");
                            joint_name[aa][1] = student.getString("s_full");
                            //name[aa] = "no";
                            aa++;
                            //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                        }


                    }

                    //array sorting
                    Arrays.sort(joint_name, new Comparator<String[]>() {
                        @Override
                        public int compare(final String[] entry1, final String[] entry2) {
                            final String time1 = entry1[0];
                            final String time2 = entry2[0];
                            return time1.compareTo(time2);
                        }
                    });

                    for(int a = 0; a < len; a++){
                        regi[a] = joint_marks[a][0];
                        marks[a] = joint_marks[a][1];
                        name[a] = joint_name[a][1];
                    }

                    Intent intent = new Intent(getApplicationContext(),upload_marks_pre.class);
                    intent.putExtra("ex_course",s_class);
                    startActivity(intent);
                    aa=0;
                    len=0;
                    pd.hide();

                } catch (JSONException e) {

                    e.printStackTrace();

                    pd.hide();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue2.add(jsonObjectRequest3);
        requestQueue2.getCache().clear();
        requestQueue2.getCache().remove(showUrl3);

    }

}
