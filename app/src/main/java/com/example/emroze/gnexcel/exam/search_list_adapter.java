package com.example.emroze.gnexcel.exam;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.emroze.gnexcel.R;

import java.util.List;

/**
 * Created by Emroze on 08-Dec-17.
 */

public class search_list_adapter  extends ArrayAdapter<search_list_element> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<search_list_element> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public search_list_adapter(Context context, int resourceId,
                               List<search_list_element> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView f_name;
        TextView name;
        TextView date;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final com.example.emroze.gnexcel.exam.search_list_adapter.ViewHolder holder;
        if (view == null) {
            holder = new com.example.emroze.gnexcel.exam.search_list_adapter.ViewHolder();
            view = inflater.inflate(R.layout.s_by_exam_list, null);
            // Locate the TextViews in listview_item.xml

            holder.name = (TextView) view.findViewById(R.id.tv_full_name_lv);
            holder.f_name = (TextView) view.findViewById(R.id.tv_hide);
            holder.date = (TextView) view.findViewById(R.id.tv_nick_name_lv);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");


            holder.name.setTypeface(type);
            holder.f_name.setTypeface(type);
            holder.date.setTypeface(type1);

            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (com.example.emroze.gnexcel.exam.search_list_adapter.ViewHolder) view.getTag();
        }
        // Capture position and set to the TextViews
        holder.name.setText(worldpopulationlist.get(position).getName());
        holder.f_name.setText(worldpopulationlist.get(position).getF_name());
        holder.date.setText(worldpopulationlist.get(position).getDate());
        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<search_list_element> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
