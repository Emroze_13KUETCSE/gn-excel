package com.example.emroze.gnexcel.exam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.emroze.gnexcel.R;

public class exam_menu extends AppCompatActivity {

    TextView tv_new,tv_pre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_menu);

        tv_new = (TextView) findViewById(R.id.tv_new);
        tv_pre = (TextView) findViewById(R.id.tv_pre);

        tv_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(),exam_name.class);
                startActivity(in);
            }
        });

        tv_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(),choose_pre_exam_list.class);
                startActivity(in);
            }
        });


    }
}
