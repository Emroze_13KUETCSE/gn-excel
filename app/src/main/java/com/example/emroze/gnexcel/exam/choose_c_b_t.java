package com.example.emroze.gnexcel.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emroze.gnexcel.R;
import com.example.emroze.gnexcel.retrofit.AppConfig;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pl.droidsonroids.gif.GifTextView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class choose_c_b_t extends AppCompatActivity {
    public static String PREFS_NAME="digits_insert";

    GifTextView g1,g2,g3;

    TextView tv_class,tv_batch,tv_7,tv_8,tv_9,tv_10,tv_7b,tv_8b,tv_9b,tv_12b,tv_3b,tv_4b,tv_5b,tv_730b,tv_time,tv_sat,tv_sun;

    Button btn_search;

    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2;

    String search_class , search_batch, sat_sun, batch_time ="";

    LinearLayout ll,ll_d;
    String BASE_URL = "http://gonitniketan.com/";


    ProgressDialog pd;


    String ex_name1="",ex_date1="",ex_class1="",ex_batch1= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_c_b_t);

        Toast.makeText(this, "choose_c_b_t", Toast.LENGTH_SHORT).show();

        pd = new ProgressDialog(choose_c_b_t.this);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        ex_name1 = preference.getString("exam_name","");
        ex_date1 = preference.getString("exam_date","");

        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        g2 = (GifTextView) findViewById(R.id.btn_gif2);
        g3 = (GifTextView) findViewById(R.id.btn_gif3);


        tv_class = (TextView) findViewById(R.id.tv_class);
        tv_batch = (TextView) findViewById(R.id.tv_batch);
        tv_7 = (TextView) findViewById(R.id.tv_7);
        tv_8 = (TextView) findViewById(R.id.tv_8);
        tv_9 = (TextView) findViewById(R.id.tv_9);
        tv_10 = (TextView) findViewById(R.id.tv_10);
        tv_7b = (TextView) findViewById(R.id.tv_7b);
        tv_8b = (TextView) findViewById(R.id.tv_8b);
        tv_9b = (TextView) findViewById(R.id.tv_9b);
        tv_12b = (TextView) findViewById(R.id.tv_12b);
        tv_3b = (TextView) findViewById(R.id.tv_3b);
        tv_4b = (TextView) findViewById(R.id.tv_4b);
        tv_5b = (TextView) findViewById(R.id.tv_5b);
        tv_730b = (TextView) findViewById(R.id.tv_730b);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);

        btn_search = (Button) findViewById(R.id.btn_search_by_class);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        tv_class.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_7.setTypeface(type1);
        tv_8.setTypeface(type1);
        tv_9.setTypeface(type1);
        tv_10.setTypeface(type1);
        tv_7b.setTypeface(type1);
        tv_8b.setTypeface(type1);
        tv_9b.setTypeface(type1);
        tv_12b.setTypeface(type1);
        tv_3b.setTypeface(type1);
        tv_4b.setTypeface(type1);
        tv_5b.setTypeface(type1);
        tv_730b.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_sat.setTypeface(type1);
        tv_sun.setTypeface(type1);

        btn_search.setTypeface(type1);

        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);

        ll = (LinearLayout) findViewById(R.id.ll);

        ll.setVisibility(View.INVISIBLE);

        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        //select class
        tv_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "7";
                tv_class.setText("Class 7");
                expandableLayout0.collapse();
            }
        });
        tv_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "8";
                tv_class.setText("Class 8");
                expandableLayout0.collapse();
            }
        });
        tv_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "9";
                tv_class.setText("Class 9");
                expandableLayout0.collapse();
            }
        });
        tv_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "1";
                tv_class.setText("Class 10");
                expandableLayout0.collapse();
            }
        });



        //select batch
        tv_7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0700am";
                tv_batch.setText("7 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0800am";
                tv_batch.setText("8 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0900am";
                tv_batch.setText("9 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "1200pm";
                tv_batch.setText("12 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0300pm";
                tv_batch.setText("3 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0400pm";
                tv_batch.setText("4 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0500pm";
                tv_batch.setText("5 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_730b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0730pm";
                tv_batch.setText("7 30 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });


        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });


        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sat";
                tv_time.setText("SAT");
                expandableLayout2.collapse();
            }
        });

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sun";
                tv_time.setText("SUN");
                expandableLayout2.collapse();
            }
        });


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batch_time = search_batch+" "+sat_sun;

                if(search_class == ""){
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);
                    Toast.makeText(choose_c_b_t.this, "At least fill up Class", Toast.LENGTH_SHORT).show();
                }
                else if(search_batch == ""  || sat_sun == ""){
                    pd.setMessage("Please wait...");
                    pd.show();
                    ex_class1 = search_class;
                    ex_batch1 = batch_time;
                    ex_name1 = ex_name1+"sk"+ex_date1+"sk"+ex_class1+"sk"+ex_batch1;

                    editor.putString("s_class",search_class);
                    editor.putString("s_batch",batch_time);
                    editor.putString("class","false");
                    editor.putString("exam_list","false");
                    editor.putString("sms","false");
                    editor.putString("exam_name",ex_name1);
                    editor.commit();

                    add_exam_name();

                    Intent i = new Intent(getApplicationContext(),upload_marks.class);
                    startActivity(i);
                    finish();
                }
                else if(search_class != "") {
                    pd.setMessage("Please wait...");
                    pd.show();
                    ex_class1 = search_class;
                    ex_batch1 = "";
                    ex_name1 = ex_name1+"sk"+ex_date1+"sk"+ex_class1+"sk"+ex_batch1;
                    editor.putString("s_class",search_class);
                    editor.putString("class","true");
                    editor.putString("exam_list","false");
                    editor.putString("sms","false");
                    editor.putString("exam_name",ex_name1);
                    editor.commit();



                    add_exam_name();

                    Intent i = new Intent(getApplicationContext(),upload_marks.class);
                    startActivity(i);
                    finish();
                    ///Toast.makeText(s_by_r_class_and_batch.this, search_class, Toast.LENGTH_SHORT).show();


                }



            }
        });
    }


    private void add_exam_name(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.add_exam api = adapter.create(AppConfig.add_exam.class);

        api.add_exam_name(
                ex_name1,
                ex_date1,
                ex_class1,
                ex_batch1,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);
                            //alter_in_sqlite();
                            Toast.makeText(choose_c_b_t.this, "Successfully Added", Toast.LENGTH_SHORT).show();

                            pd.hide();

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
