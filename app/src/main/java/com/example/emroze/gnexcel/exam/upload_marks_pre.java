package com.example.emroze.gnexcel.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emroze.gnexcel.R;
import com.example.emroze.gnexcel.URI_to_Path;
import com.example.emroze.gnexcel.retrofit.AppConfig;
import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class upload_marks_pre extends AppCompatActivity {
    public static String PREFS_NAME="digits_insert";

    TextView t,t1;
    Button upload,select;
    File inputFile;
    private final static int REQUEST_CODE = 1001;
    String BASE_URL = "http://gonitniketan.com/";
    String[] saved_column,check,pd_check;

    String[][] row_col;
    ProgressDialog pd;

    public int row=0;

    final Handler handler = new Handler();
    final Timer timer = new Timer();

    int i = 0;

    com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView animatedCircleLoadingView;

    public int count = 0;

    public int count1 = 0;

    int check_bit = 0;

    int error_check = 0;

    int count_error = 0;

    String ex_name="";

    String course="";

    String classs="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_marks_pre);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Intent intent = getIntent();
        classs = intent.getStringExtra("ex_course");

        ex_name = preference.getString("exam_name","");

        String search = "marks_count_"+classs+"_"+ex_name;

        try{
            count1 = Integer.valueOf(preference.getString(search,""))+1;
        }catch (NumberFormatException e){
            count1=0;
        }
        count1=0;


        Toast.makeText(this, String.valueOf(count1), Toast.LENGTH_SHORT).show();

        animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);
        pd = new ProgressDialog(this);
        t = (TextView) findViewById(R.id.textView);
        t1 = (TextView) findViewById(R.id.textView1);
        upload = (Button) findViewById(R.id.btn_upload);
        select = (Button) findViewById(R.id.btn_select);


        final String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/std.xls";

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upload();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(preference.getString("thread",null) == null || preference.getString("thread","").equals("on")){
                    editor.putString("thread","off");
                    editor.commit();
                    //pd.setMessage("Uploading....");

                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    try{
                        animatedCircleLoadingView.startDeterminate();
                    }
                    catch (IllegalStateException e){

                    }

                    readExcelData();
                }



            }
        });

        final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {


                        if(preference.getString("animation","").equals("on")){
                            if (count_error == 10) {
                                animatedCircleLoadingView.stopFailure();

                                editor.putString("thread","on");
                                editor.putInt("count",count);
                                editor.putString("marks_count_"+course+"_"+ex_name,String.valueOf(count+count1));
                                editor.putString("fail","on");
                                editor.putString("animation","off");
                                editor.commit();

                            }

                            if(count < row){
                                int f = (count*100)/row;
                                animatedCircleLoadingView.setPercent(f);
                            }

                            if(count >= (row) && count != 0 && row != 0){
                                animatedCircleLoadingView.stopOk();
                                editor.putString("thread","on");
                                editor.putString("animation","off");
                                editor.commit();

                            }
                            Toast.makeText(getApplicationContext(), String.valueOf(count)+"/"+String.valueOf(row), Toast.LENGTH_SHORT).show();

                        }

                    }
                });
            }
        };
        timer.schedule(timerTask, 0,1000);

    }

    private void readExcelData() {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        //decarle input file
        Toast.makeText(this, "Total ", Toast.LENGTH_SHORT).show();
        try {
            InputStream inputStream = new FileInputStream(inputFile);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

            row=rowsCount-1;

            Toast.makeText(this, "Total "+String.valueOf(row), Toast.LENGTH_SHORT).show();

            //row = row - count1 + 1;

            Row row1 = sheet.getRow(0);
            int cellsCount = row1.getPhysicalNumberOfCells();

            //Toast.makeText(this, "cells count "+String.valueOf(cellsCount), Toast.LENGTH_SHORT).show();

            row_col = new String[row][2];


            for(int i = 0; i < row ; i++)
            {
                row_col[i][0] = "0";
                row_col[i][1] = "0";
            }

            //Row row11 = sheet.getRow(row);
            //String value33 = getCellAsString(row11, 0, formulaEvaluator);

            //Toast.makeText(this, "Regi "+value33, Toast.LENGTH_SHORT).show();

            int flag = 1;
            int flag_count=0;
            //outter loop, loops through rows
            for (int r = count1 ; r < rowsCount; r++) {

                Row row2 = sheet.getRow(r);
                //inner loop, loops through columns
                for (int c = 0; c < cellsCount; c++) {
                    //handles if there are to many columns on the excel sheet.
                    String value = getCellAsString(row2, c, formulaEvaluator);
                    if(c == 0){

                        double numm = 0.0;
                        try{
                            numm = Double.valueOf(value);
                        }
                        catch (NumberFormatException e){
                            flag = 0;
                            flag_count++;
                            //Toast.makeText(this, "Here", Toast.LENGTH_SHORT).show();
                        }

                        if(flag == 1){
                            String num = String.format("%.1f",numm);
                            numm = Double.valueOf(num);
                            int nummm = Integer.valueOf((int) numm);
                            num = String.valueOf(nummm);
                            value = String.valueOf(num);
                            row_col[r-1][0]=value;
                        }

                    }
                    else if(c == 3 && flag == 1){
                        row_col[r-1][1]=value;
                    }

                }
                flag = 1;

            }

        }catch (FileNotFoundException e) {

            pd.hide();
            //pd.hide();
            animatedCircleLoadingView.stopFailure();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
            editor.putString("thread","on");
            editor.commit();
            Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
            pd.hide();
            animatedCircleLoadingView.stopFailure();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
            editor.putString("thread","on");
            editor.commit();

        }
        catch (NullPointerException e){
            pd.hide();
            animatedCircleLoadingView.stopFailure();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
            editor.putString("thread","on");
            editor.commit();
        }

        check_bit = 1;
        error_check = 0;

        if(row_col[0][0].charAt(2) == '7'){
            course="7";
        }
        else if(row_col[0][0].charAt(2) == '8'){
            course = "8";
        }
        else if(row_col[0][0].charAt(2) == '9'){
            course = "9";
        }
        else if(row_col[0][0].charAt(2) == '1'){
            course = "1";
        }

        editor.putString("course",course);
        editor.putString("animation","on");
        editor.commit();

        Thread_t(row_col);

        //Toast.makeText(this, "regi "+row_col[0][0]+" "+row_col[row-1][0], Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, "Marks "+row_col[0][1]+" "+row_col[row-1][1], Toast.LENGTH_SHORT).show();

    }


    private void Thread_t(final String[][] data){


        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(50);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(check_bit == 1 && count < row){
                                    check_bit = 0;
                                    count_error = 0;
                                    insert_std(count,data);
                                }
                                if(error_check == 1 && count_error < 10){
                                    error_check = 0;
                                    insert_std(count,data);
                                }
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

    private void insert_std(final int indexx,String[][] data){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_marks api = adapter.create(AppConfig.update_marks.class);

        if(data[indexx][0] != "0" && data[indexx][1] != "0"){
            //Toast.makeText(this, String.valueOf(indexx), Toast.LENGTH_SHORT).show();

            if(data[indexx][1] == ""){
                data[indexx][1] = "-1";
            }

            api.update_exam_marks(
                    data[indexx][0],
                    data[indexx][1],
                    ex_name,
                    new Callback<Response>() {
                        @Override
                        public void success(Response result, Response response) {

                            try {

                                BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                                String resp;
                                resp = reader.readLine();
                                Log.d("success", "" + resp);
                                //Toast.makeText(getApplicationContext(), "Successfully registered "+String.valueOf(indexx), Toast.LENGTH_SHORT).show();
                                count++;
                                editor.putInt("count",count);
                                editor.putString("marks_count_"+course+"_"+ex_name,String.valueOf(count+count1));
                                editor.commit();

                                check_bit = 1;
                                error_check = 0;

                            } catch (IOException e) {
                                Log.d("Exception", e.toString());
                                count_error++;
                                error_check = 1;

                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {

                            count_error++;
                            error_check = 1;
                            //Toast.makeText(getApplicationContext(), "No internet "+String.valueOf(indexx), Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }else{
            //Toast.makeText(this, "else "+String.valueOf(indexx), Toast.LENGTH_SHORT).show();

            count++;

            editor.putInt("count",count);
            editor.putString("marks_count_"+course+"_"+ex_name,String.valueOf(count+count1));
            editor.commit();

            check_bit = 1;
            error_check = 0;
        }

    }


    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if(HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("MM/dd/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = ""+numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
        }
        return value;
    }


    private void upload() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent,
                "File"), REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (resultCode != RESULT_OK || data == null) return;
        // Check which request we're responding to
        if (requestCode == REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                //Image URI received
                inputFile = new File(URI_to_Path.getPath(getApplication(), data.getData()));
                if (inputFile != null) {
                    //new UploadTask(DropboxClient.getClient(ACCESS_TOKEN), file, getApplicationContext()).execute();
                    t.setText(String.valueOf(inputFile.getPath()));
                    if(!preference.getString("path","").equals(String.valueOf(inputFile.getPath()))){
                        t1.setText("");
                        count=0;
                    }
                    editor.putString("path",String.valueOf(inputFile.getPath()));
                    editor.commit();
                    //readExcelData();
                }
            }
        }
    }
}
