package com.example.emroze.gnexcel;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emroze.gnexcel.retrofit.AppConfig;
import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;


import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.IllegalFormatException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.emroze.gnexcel.R.styleable.AnimatedCircleLoadingView;

public class MainActivity extends AppCompatActivity {

    public static String PREFS_NAME="digits_insert";

    TextView t,t1;
    Button upload,select;
    File inputFile;
    private final static int REQUEST_CODE = 1001;
    String BASE_URL = "http://gonitniketan.com/";
    String[] saved_column,check,pd_check;

    String[][] row_col;
    ProgressDialog pd;

    public int row=0;

    final Handler handler = new Handler();
    final Timer timer = new Timer();

    int i = 0;

    com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView animatedCircleLoadingView;

    public int count = 0;

    int check_bit = 0;

    int error_check = 0;

    int count_error = 0;

    Thread thread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);
        pd = new ProgressDialog(this);
        t = (TextView) findViewById(R.id.textView);
        t1 = (TextView) findViewById(R.id.textView1);
        upload = (Button) findViewById(R.id.btn_upload);
        select = (Button) findViewById(R.id.btn_select);

        //checkFilePermissions();
        final String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/std.xls";

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upload();
            }
        });

        if(preference.getString("fail","").equals("on")){

            editor.putString("fail","off");
            editor.commit();
            t1.setText("Previous data register failed....");
            t.setText(preference.getString("path",""));
            count = preference.getInt("count",0);
        }

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(preference.getString("thread",null) == null || preference.getString("thread","").equals("on")){
                    editor.putString("thread","off");
                    editor.commit();
                    pd.setMessage("Uploading....");

                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    //pd.show();
                    try{
                        animatedCircleLoadingView.startDeterminate();
                    }
                    catch (IllegalStateException e){

                    }

                    readExcelData();
                }

            }
        });

        final int[] a = {0};

        final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {

                        /*for(i = 0; i < row ; i++){
                            //Toast.makeText(MainActivity.this, pd_check[i]+" "+String.valueOf(row), Toast.LENGTH_SHORT).show();
                            if(pd_check[i] == "false"){
                                break;
                            }

                        }
                        int ii = row;
                        //Toast.makeText(MainActivity.this, String.valueOf(i), Toast.LENGTH_SHORT).show();
                        if(i == ii){
                            pd.hide();
                            animatedCircleLoadingView.stopOk();
                            //Toast.makeText(MainActivity.this, "done", Toast.LENGTH_SHORT).show();
                        }
                        a[0]++;*/

                        if(preference.getString("animation","").equals("on")){
                            if (count_error == 5) {
                                animatedCircleLoadingView.stopFailure();
                                editor.putString("thread","on");
                                editor.putInt("count",count);
                                editor.putString("fail","on");
                                editor.putString("animation","off");
                                editor.commit();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        System.exit(0);
                                    }
                                }, 4000);
                            }

                            if(count < row){
                                int f = (count*100)/row;
                                animatedCircleLoadingView.setPercent(f);
                            }

                            if(count >= row){
                                animatedCircleLoadingView.stopOk();
                                editor.putString("thread","on");
                                editor.putString("animation","off");
                                editor.commit();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        thread.interrupt();
                                        finish();
                                    }
                                }, 4000);
                            }
                        }

                    }
                });
            }
        };
        timer.schedule(timerTask, 0,500);
    }

    private void readExcelData() {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        //decarle input file
        try {
            InputStream inputStream = new FileInputStream(inputFile);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

            row=rowsCount-1;
            Toast.makeText(this, "Total "+String.valueOf(rowsCount), Toast.LENGTH_SHORT).show();
            check = new String[rowsCount-1];
            pd_check = new String[rowsCount-1];
            saved_column = new String[rowsCount-1];
            Row row1 = sheet.getRow(0);
            int cellsCount = row1.getPhysicalNumberOfCells();

            //Toast.makeText(this, "cells count "+String.valueOf(cellsCount), Toast.LENGTH_SHORT).show();

            row_col = new String[row][cellsCount];

            String[] store = new String[cellsCount];

            //outter loop, loops through rows
            for (int r = 1; r < rowsCount; r++) {
                Row row2 = sheet.getRow(r);
                //inner loop, loops through columns
                for (int c = 0; c < cellsCount; c++) {
                    //handles if there are to many columns on the excel sheet.
                    String value = getCellAsString(row2, c, formulaEvaluator);
                    if(c == 6){
                        double numm = 0.0;
                        try{
                            numm = Double.valueOf(value);
                        }
                        catch (NumberFormatException e){
                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(100);
                        }

                        String num = String.format("%.1f",numm);
                        numm = Double.valueOf(num);
                        int nummm = Integer.valueOf((int) numm);
                        num = String.valueOf(nummm);
                        value = String.valueOf(num);
                    }

                    row_col[r-1][c]=value;
                    if(c == 0){
                        saved_column[r-1] = value;
                    }
                    else {
                        saved_column[r-1]= saved_column[r-1]+";"+value;
                    }

                    store[c] = value;
                    //Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
                }
                check[r-1]="false";
                pd_check[r-1]="false";
                //Thread_t(r-1,row_col);
                //insert_std(store);
                //Toast.makeText(this, saved_column[r], Toast.LENGTH_SHORT).show();
            }

            check_bit = 1;
            error_check = 0;
            editor.putString("animation","on");
            editor.commit();




            Thread_t(row_col);

        }catch (FileNotFoundException e) {

            pd.hide();
            animatedCircleLoadingView.stopFailure();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
            editor.putString("thread","on");
            editor.commit();
        } catch (IOException e) {
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
            pd.hide();
            editor.putString("thread","on");
            editor.commit();
            animatedCircleLoadingView.stopFailure();
        }
        catch (NullPointerException e){
            editor.putString("thread","on");
            editor.commit();
            animatedCircleLoadingView.stopFailure();
        }
    }


    private void Thread_t(final String[][] data){


        thread = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(50);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                /*if(check[index] == "false"){
                                    check[index] = "true";
                                    //Toast.makeText(MainActivity.this, String.valueOf(index), Toast.LENGTH_SHORT).show();
                                    //Toast.makeText(MainActivity.this, d[index][0], Toast.LENGTH_SHORT).show();
                                    insert_std(index,d);
                                }*/

                                if(check_bit == 1 && count < row){
                                    check_bit = 0;
                                    count_error = 0;
                                    insert_std(count,data);
                                }
                                if(error_check == 1 && count_error < 5){
                                    error_check = 0;
                                    insert_std(count,data);
                                }

                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();
    }


    private void insert_std(final int indexx,String[][] data){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();



        try {
            double num = Double.parseDouble(data[indexx][0]);
            System.out.println(num);

            if(!data[indexx][1].equals("")){
                RestAdapter adapter = new RestAdapter.Builder()
                        .setEndpoint(BASE_URL) //Setting the Root URL
                        .build();

                AppConfig.insert api = adapter.create(AppConfig.insert.class);

                String course="";
                if(data[indexx][4].equals("test")){
                    course="11";
                }
                else {
                    if(data[indexx][0].charAt(2) == '7'){
                        course="7";
                    }
                    else if(data[indexx][0].charAt(2) == '8'){
                        course = "8";
                    }
                    else if(data[indexx][0].charAt(2) == '9'){
                        course = "9";
                    }
                    else if(data[indexx][0].charAt(2) == '1'){
                        course = "1";
                    }
                    else if(data[indexx][0].charAt(2) == '0'){
                        course = "0";
                    }
                }

                System.out.println(data[indexx][8]+" : "+data[indexx][0]);
                //Toast.makeText(this,String.valueOf(indexx), Toast.LENGTH_SHORT).show();
                //0521346
                api.insertData(
                        data[indexx][0],
                        data[indexx][5],
                        data[indexx][2],
                        data[indexx][1],
                        data[indexx][3],
                        data[indexx][4],
                        data[indexx][6],
                        course,
                        data[indexx][7],
                        data[indexx][8],
                        new Callback<Response>() {
                            @Override
                            public void success(Response result, Response response) {

                                try {

                                    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                                    String resp;
                                    resp = reader.readLine();
                                    Log.d("success", "" + resp);
                                    //Toast.makeText(getApplicationContext(), "Successfully registered "+String.valueOf(indexx), Toast.LENGTH_SHORT).show();
                                    pd_check[indexx] = "true";
                                    count++;
                                    editor.putInt("count",count);
                                    editor.commit();
                                    check_bit = 1;
                                    error_check = 0;
                                    //Toast.makeText(MainActivity.this, "Success "+pd_check[indexx]+" "+String.valueOf(indexx), Toast.LENGTH_SHORT).show();
                                    //pd.hide();
                                    //JSONObject jObj = new JSONObject(resp);

                                } catch (IOException e) {
                                    Log.d("Exception", e.toString());
                                    check[indexx] = "false";
                                    count_error++;
                                    error_check = 1;
                                    System.out.println("error io ex\n\n");
                                    //Toast.makeText(getApplicationContext(), e.toString()+String.valueOf(indexx), Toast.LENGTH_SHORT).show();
                                    //pd.hide();
                                    //Toast.makeText(getApplicationContext(), "Please try again "+String.valueOf(indexx), Toast.LENGTH_LONG).show();

                                }

                            }

                            @Override
                            public void failure(RetrofitError e) {
                                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                                //pd.hide();
                                check[indexx] = "false";
                                count_error++;
                                error_check = 1;
                                Log.d("Exception", e.toString());
                                System.out.println("error retrofit\n\n");
                                //Toast.makeText(getApplicationContext(), "No internet "+String.valueOf(indexx), Toast.LENGTH_LONG).show();
                            }
                        }
                );
            }
            else {
                count++;
                editor.putInt("count",count);
                editor.commit();
                check_bit = 1;
                error_check = 0;
            }
        }catch (NumberFormatException e){

            count++;
            editor.putInt("count",count);
            editor.commit();
            check_bit = 1;
            error_check = 0;
        }




    }


    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if(HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("MM/dd/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = ""+numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
        }
        return value;
    }

    private void upload() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent,
                "File"), REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (resultCode != RESULT_OK || data == null) return;
        // Check which request we're responding to
        if (requestCode == REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                //Image URI received
                inputFile = new File(URI_to_Path.getPath(getApplication(), data.getData()));
                if (inputFile != null) {
                    //new UploadTask(DropboxClient.getClient(ACCESS_TOKEN), file, getApplicationContext()).execute();
                    t.setText(String.valueOf(inputFile.getPath()));
                    if(!preference.getString("path","").equals(String.valueOf(inputFile.getPath()))){
                        t1.setText("");
                        count=0;
                    }
                    editor.putString("path",String.valueOf(inputFile.getPath()));
                    editor.commit();
                    //readExcelData();
                }
            }
        }
    }
}
