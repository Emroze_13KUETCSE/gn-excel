package com.example.emroze.gnexcel.exam;

/**
 * Created by Emroze on 08-Dec-17.
 */

public class search_list_element {
    String f_name;
    String name;
    String date;

    public search_list_element(String f_name,String name ,String date) {
        this.f_name = f_name;
        this.name = name;
        this.date = date;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
