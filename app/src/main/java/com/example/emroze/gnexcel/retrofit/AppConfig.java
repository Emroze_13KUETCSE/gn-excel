package com.example.emroze.gnexcel.retrofit;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by emroze on 6/17/17.
 */

public class AppConfig {
    public interface insert {
        @FormUrlEncoded
        @POST("/android/insertData.php")
        void insertData(
                @Field("regi") String regi,
                @Field("batch_time") String batch_time,
                @Field("s_nick") String s_nick,
                @Field("s_full") String s_full,
                @Field("father_name") String father_name,
                @Field("school") String school,
                @Field("fa_contact") String fa_contact,
                @Field("course") String course,
                @Field("payment") String payment,
                @Field("active") String active,

                Callback<Response> callback);
    }

    public interface add_exam {
        @FormUrlEncoded
        @POST("/android/alter_table.php")
        void add_exam_name(
                @Field("column") String exam_name,
                @Field("exam_date") String exam_date,
                @Field("class") String class_a,
                @Field("batch") String batch,
                Callback<Response> callback);

    }
    public interface update_marks {
        @FormUrlEncoded
        @POST("/android/update_exam_marks.php")
        void update_exam_marks(
                @Field("regi") String regi,
                @Field("marks") String marks,
                @Field("exam_name") String exam_name,
                Callback<Response> callback);
    }
}
